# gifs.nz static site codebase

To run it and test locally, just clone the repo, and run the index file in your browser. CORS is setup to allow anyone to query the images, so it's easy to test the code locally.

## Stateless Only Requirements

The website should ALWAYS be just a stateless site. It's a static website, with most of the logic happening in the js and with all the images and content being served (via a proxy for DNS and TLS) from object storage.

This site will never POST to remove servers, and any semi-stateful functionality should be entirely local and js based. We may expand what is hosted in object storage, but any GET requests will always be to object storage

## Future plans

- Local memory based favourites: The ability to select your own favourites, and list/search just your favourites.
- Local memory most used gifs: Keep stats about which gifs you copy/share the most and let people see a list of them sorted by most used.
- ability to share favourites: an entirely in url favourites list that when linked and loaded allows someone to add those favourites to their own local list. May require some hashing to avoid crazy url lengths, or using gif etags.

