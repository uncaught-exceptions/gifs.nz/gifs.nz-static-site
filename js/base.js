const url = "https://gifs.nz/gifs/";
const initial_images_loaded = 9;
const images_per_load = 3;

var gif_objects = [];
var unloaded_images = [];


function sort_by_key(array, key) {
  return array.sort(function(a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
  });
}

function get_url_vars() {
  var from_url = window.location.href;
  var vars = {};
  if (from_url.includes("?")) {
    var hashes = from_url.split("?")[1];
    var hash = hashes.split('&');

    for (var i = 0; i < hash.length; i++) {
    params=hash[i].split("=");
    vars[params[0]] = decodeURI(params[1]);
    }
  }
  return vars;
}

function copy_link(clicked_id) {
  var button = document.getElementById(clicked_id);
  var copyText = document.getElementById(button.dataset.input);
  copyText.select();
  document.execCommand("copy");
  copyText.blur();
}

function make_gif_thumb(gif){
  var clean_name = gif.name.replace('/', '-').replace('.', '-');
  var gif_thumb_div = document.createElement('div');
  gif_thumb_div.className = "gif-thumb";
  gif_thumb_div.innerHTML = (
      "<a href='" + url + gif.name + "' target='_blank'>" +
        "<img src='" + url + gif.name + "'>" +
      "</a>" +
      "<div class='input-group'>" +
        "<input type='text' class='form-control' aria-describedby='button-"+ gif.name +"' value=" + url + gif.name + " id='input-"+ clean_name + "'>" +
        "<div class='input-group-append'>" +
          "<button class='btn btn-outline-secondary' type='button' onclick='copy_link(this.id)' id='button-"+ clean_name + "' data-input='input-"+ clean_name + "'>copy</button>" +
        "</div>" +
      "</div>"
  );
  return gif_thumb_div;
}

function redraw_pages(gifs) {
  var filtered_gifs = [];
  var query_vars = get_url_vars();
  if (query_vars.search) {
    var search_terms = query_vars.search.split(' ');
    gifs.forEach( function( gif, index ) {
      if(gif){
        var match = true;
        search_terms.forEach( function( term, index ) {
          if (!gif.name.toLowerCase().includes(term.toLowerCase())){
            match = false;
          }
        });
        if (match) {
          filtered_gifs.push(gif);
        }
      }
    });
  } else {
    filtered_gifs = gifs;
  }

  var gif_list = document.getElementById('gif-list');
  gif_list.innerHTML = ''
  unloaded_images = [];

  var image_number = 1;
  filtered_gifs.forEach( function( gif, index ) {
    if (image_number <= initial_images_loaded) {
      gif_list.appendChild(make_gif_thumb(gif));
    } else {
      unloaded_images.push(gif);
    }
    image_number++;
  });

  if (unloaded_images.length) {
    document.getElementById("scroll-message").style.display = "block";
  } else {
    document.getElementById("scroll-message").style.display = "none";
  }
}

function load_next(){
  var gif_list = document.getElementById('gif-list');
  var i;
  for (i = 0; i < images_per_load && unloaded_images.length; i++) {
    gif_list.appendChild(make_gif_thumb(unloaded_images[0]));
    unloaded_images.splice(0, 1);
  }
  if (!unloaded_images.length) {
    document.getElementById("scroll-message").style.display = "none";
  }
}

function load_next_scroll(){
  if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
    load_next()
  }
}

function page_load (){
  fetch(url, {
      method: 'get',
      headers: {
        "Accept": "application/json"
      }
  }).then(function (response) {
    response.json().then(function(data) {
      data.forEach( function( val, index ) {
        if (val.content_type === "image/gif") {
          gif_objects.push(val);
        }
      });
      var query_vars = get_url_vars();
      if (query_vars.search) {
        document.getElementById('search_input').value = query_vars.search;
      }
      gif_objects = sort_by_key(gif_objects, "last_modified").reverse();
      redraw_pages(gif_objects);
    });
  });
}

window.onload = page_load;
window.onscroll = load_next_scroll;
document.getElementById("scroll-message").onclick = load_next

document.getElementById("search_input").onkeyup = function(e){
  var search_input = document.getElementById('search_input');
  if (search_input.value) {
    history.replaceState(null, null, "?search=" + search_input.value);
  } else {
    history.replaceState(null, null, window.location.href.split("?")[0]);
  }
  redraw_pages(gif_objects);
};